#!/usr/bin/env python2.7

import urllib, urlparse, json, datetime, os
from collections import namedtuple

DNSRecord = namedtuple('DNSRecord', ['record', 'value', 'type', 'editable',
                                     'zone', 'account_id', 'comment'])

with open(os.path.expanduser('~/dynamicdns.apikey')) as apikey:
    API_KEY = apikey.read().strip()

TIMESTAMP = 'Updated at ' + str(datetime.datetime.now())

def dh_api (cmd, **args):
    args['key'] = API_KEY
    args['cmd'] = cmd
    args['format'] = 'json'
    result = json.load(urllib.urlopen('https://api.dreamhost.com/', data=urllib.urlencode(args)))
    return result.get('result'), result.get('data')

def get_records ():
    result, data = dh_api('dns-list_records')
    if data:
        for record in data:
            record['editable'] = record['editable'] == '1'
            yield DNSRecord(**record)

def remove_record (record):
    dh_api('dns-remove_record', **record._asdict())

def add_record (hostname, ip):
    dh_api('dns-add_record', record=hostname, type='A', value=ip, comment=TIMESTAMP)

def update (hostname, ip):
    need_to_add = True
    for r in get_records():
        if r.record == hostname and r.editable and r.type == 'A':
            if r.value != ip: remove_record(r)
            else: need_to_add = False
    if need_to_add:
        add_record(hostname, ip)
        return True
    else:
        return False

print 'Content-Type: text/html'
print ''

query = urlparse.parse_qs(os.environ.get('QUERY_STRING', ''))

hostname = query.get('hostname')
if hostname: hostname = hostname[0]

ip = query.get('myip')
if ip: ip = ip[0]
else: ip = os.environ.get('REMOTE_ADDR')

if not hostname or hostname != os.environ.get('REMOTE_USER'):
    print 'nohost'
elif not ip:
    print 'good 127.0.0.1'
elif update(hostname, ip):
    print 'good'
else:
    print 'nochg'
